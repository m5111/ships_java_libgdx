package com.michal5111.ships.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.michal5111.ships.ShipsGame;

/**
 * Created by Michal on 08.11.2017.
 */

class PlayerWinScreen implements Screen {
    private final Stage stage;
    private final Table table;
    private final TextButton textButton;
    private final ShipsGame shipsGame;

    public PlayerWinScreen(ShipsGame shipsGame,String message) {
        this.shipsGame = shipsGame;
        textButton = new TextButton(message,shipsGame.getStylist().getTextButtonStyle());
        table = new Table();
        table.setFillParent(true);
        table.add(textButton).expand().bottom().center().padRight(Gdx.graphics.getWidth()/5f).padLeft(Gdx.graphics.getWidth()/5f).height(Gdx.graphics.getHeight()/5f).fillX();
        stage = new Stage();
        stage.addActor(table);
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {
        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dispose();
                shipsGame.create();
            }
        });
    }

    @Override
    public void render(float delta) {
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        //Noting for now
    }

    @Override
    public void pause() {
        //Noting for now
    }

    @Override
    public void resume() {
        //Noting for now
    }

    @Override
    public void hide() {
        //Noting for now
    }

    @Override
    public void dispose() {
        textButton.remove();
        stage.dispose();
        table.remove();
        for (int i = 0; i < 2; i++)
            shipsGame.getBoards()[i].getBoardTable().remove();
    }
}

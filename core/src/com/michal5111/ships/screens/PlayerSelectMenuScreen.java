package com.michal5111.ships.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.michal5111.ships.Computer;
import com.michal5111.ships.Human;
import com.michal5111.ships.ShipsGame;

/**
 * Created by Michal on 07.11.2017.
 */

class PlayerSelectMenuScreen implements Screen {

    private final ShipsGame shipsGame;

    private int id;
    private final Stage stage;
    private final Table table;
    private final TextButton computerButton;
    private final TextButton humanButton;
    private final Label label;
    private static final String SELECT_PLAYER = "selectPlayer";

    public PlayerSelectMenuScreen(ShipsGame shipsGame) {
        Image background = new Image(new Texture("backgroundMenu.png"));
        background.setBounds(0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        this.shipsGame = shipsGame;
        computerButton = new TextButton(shipsGame.getBundle().get("computer"),shipsGame.getStylist().getTextButtonStyle());
        humanButton = new TextButton(shipsGame.getBundle().get("human"), shipsGame.getStylist().getTextButtonStyle());
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        Label.LabelStyle labelStyle = new Label.LabelStyle(shipsGame.getStylist().getFont(), Color.RED);
        id = 0;
        label = new Label(shipsGame.getBundle().format(SELECT_PLAYER,(id+1)),labelStyle);
        label.setFontScale(Gdx.graphics.getDensity()*2);
        table = new Table();
        Table textTable = new Table();
        textTable.setBounds(0,Gdx.graphics.getHeight()/2f,Gdx.graphics.getWidth(),Gdx.graphics.getHeight()/2f);
        textTable.add(label);
        table.setFillParent(true);
        table.add(computerButton).expand();
        table.add(humanButton).expand();
        stage.addActor(background);
        stage.addActor(textTable);
        stage.addActor(table);
    }

    @Override
    public void show() {
        computerButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                shipsGame.getPlayers()[id] = new Computer(shipsGame, shipsGame.getBoards(),id++);
                label.setText(shipsGame.getBundle().format(SELECT_PLAYER,id+1));
            }
        });
        humanButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                shipsGame.getPlayers()[id] = new Human(shipsGame, shipsGame.getBoards(),id++);
                label.setText(shipsGame.getBundle().format(SELECT_PLAYER,id+1));
            }
        });
    }

    @Override
    public void render(float delta) {
        if(id == 2) {
            shipsGame.getPlayers()[0].setShips();
            shipsGame.getPlayers()[1].setShips();
            dispose();
            shipsGame.setScreen(new SwitchPlayerScreen(shipsGame, shipsGame.getBundle().format("playerTurn",(shipsGame.getTurn() + 1))));
        }
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        //Nothing for now
    }

    @Override
    public void pause() {
        //Nothing for now
    }

    @Override
    public void resume() {
        //Nothing for now
    }

    @Override
    public void hide() {
        //Nothing for now
    }

    @Override
    public void dispose() {
        computerButton.remove();
        humanButton.remove();
        table.remove();
        stage.dispose();
        label.remove();
    }
}

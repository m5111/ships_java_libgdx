package com.michal5111.ships.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.TimeUtils;
import com.michal5111.ships.HitResult;
import com.michal5111.ships.Player;
import com.michal5111.ships.Point;
import com.michal5111.ships.ShipsGame;
import com.michal5111.ships.events.PlayerSelectedFieldEventListener;
import com.michal5111.ships.events.SelectFieldEventListener;

/**
 * Created by Michal on 07.11.2017.
 */

public class PlaySelectFieldScreen implements Screen {

    private Table textTable;
    private long millisStart;
    private Label selectedFieldLabel;
    private Point field;
    private final Player opponent;
    private final Player player;
    private Label resultLabel;
    private final ShipsGame shipsGame;
    private static final int DELAY = 1000;
    private Stage stage;
    private Boolean selected;
    private Table menuTable;
    private Button exitButton;
    private Boolean pause;
    enum result {
        NEXT_ROUND, SWITCH_ROUND, WIN
    }
    private result playResult;
    private TextButton showPlayersBoard;
    private Table playersBoardTable;

    PlaySelectFieldScreen(ShipsGame game, final Player player, final Player opponent) {
        this.shipsGame = game;
        this.opponent = opponent;
        this.player = player;
        selected = false;
        pause = false;
        setupExitMenu();
        setupTextTable();
        setupStage();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {
        shipsGame.incrementRounds();
        shipsGame.getPlayers()[shipsGame.getTurn()].fire(new SelectFieldEventListener.SelectFieldEvent(stage));
    }

    @Override
    public void render(float delta) {
        stage.draw();
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK)) {
            if (!pause) {
                pause();
            } else {
                resume();
            }
        }
        if (TimeUtils.millis() - millisStart > DELAY && selected && !pause) {
            switch (playResult) {
                case NEXT_ROUND:
                    dispose();
                    shipsGame.setScreen(new PlaySelectFieldScreen(shipsGame, player, opponent));
                    break;
                case SWITCH_ROUND:
                    dispose();
                    shipsGame.setScreen(new SwitchPlayerScreen(shipsGame, shipsGame.getBundle().format("playerTurn",(shipsGame.getTurn() + 1))));
                    break;
                case WIN:
                    dispose();
                    shipsGame.setScreen(new PlayerWinScreen(shipsGame,shipsGame.getBundle().format("playerWins",(player.geId() + 1))));
                    break;
            }
        }
    }

    @Override
    public void resize(int width, int height) {
        //Nothing for now
    }

    @Override
    public void pause() {
        pause = true;
        stage.addActor(menuTable);
    }

    @Override
    public void resume() {
        pause = false;
        millisStart = TimeUtils.millis();
        menuTable.remove();
    }

    @Override
    public void hide() {
        pause();
    }

    @Override
    public void dispose() {
        resultLabel.remove();
        selectedFieldLabel.remove();
        stage.dispose();
        textTable.remove();
    }

    private result play(Player player, Player opponent) {
        selectedFieldLabel.setText(field.toString());
        HitResult result = player.hit(field);
        if (opponent.didLose()) {
            return PlaySelectFieldScreen.result.WIN;
        } else switch (result) {
            case HIT:
                resultLabel.setText(shipsGame.getBundle().get("hit"));
                return PlaySelectFieldScreen.result.NEXT_ROUND;
            case HIT_AND_SUNK:
                resultLabel.setText(shipsGame.getBundle().get("hitAndSunk"));
                return PlaySelectFieldScreen.result.NEXT_ROUND;
            case MISS:
                resultLabel.setText(shipsGame.getBundle().get("miss"));
                shipsGame.switchTurn();
                return PlaySelectFieldScreen.result.SWITCH_ROUND;
        }
        throw new IllegalStateException();
    }

    private void setupExitMenu() {
        exitButton = new Button(shipsGame.getStylist().getButtonStyle());
        exitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pause();
            }
        });
        exitButton.setPosition(Gdx.graphics.getWidth()-exitButton.getWidth()-20,Gdx.graphics.getHeight()-exitButton.getHeight()-20);
        exitButton.setScale(Gdx.graphics.getDensity());
        menuTable = new Table();
        menuTable.background(shipsGame.getStylist().getSkin().getDrawable("button_01"));
        menuTable.setBounds(100,100,Gdx.graphics.getWidth()-200f,Gdx.graphics.getHeight()-200f);
        TextButton exitMenu = new TextButton(shipsGame.getBundle().get("menu"),shipsGame.getStylist().getTextButtonStyle());
        exitMenu.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dispose();
                shipsGame.create();
            }
        });
        TextButton settingsButton = new TextButton(shipsGame.getBundle().get("settings"),shipsGame.getStylist().getTextButtonStyle());
        settingsButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                PlaySelectFieldScreen.this.shipsGame.setScreen(new SettingsScreen(shipsGame,PlaySelectFieldScreen.this,stage));
            }
        });
        Button exitTableMenuButton = new Button(shipsGame.getStylist().getButtonStyle());
        exitTableMenuButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                resume();
            }
        });
        menuTable.add(exitTableMenuButton).right().top().expand().row();
        menuTable.add(settingsButton).row();
        menuTable.add(exitMenu);
    }

    private void setupTextTable() {
        Label.LabelStyle labelStyle = new Label.LabelStyle(shipsGame.getStylist().getFont(), Color.RED);
        selectedFieldLabel = new Label("", labelStyle);
        resultLabel = new Label(shipsGame.getBundle().get("selectField"), labelStyle);
        showPlayersBoard = new TextButton(shipsGame.getBundle().get("myBoard"),shipsGame.getStylist().getTextButtonStyle());
        showPlayersBoard.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                textTable.add(player.getMyBoard().getBoardTable());
            }
        });
        Label shipsLeftLabel = new Label(shipsGame.getBundle().format("opponentShipsCount",opponent.getUnsunkenShipsCount()),labelStyle);
        Label yourShipsLeftLabel = new Label(shipsGame.getBundle().format("playerShipsCount",player.getUnsunkenShipsCount()),labelStyle);
        Label roundNumberLabel = new Label(shipsGame.getBundle().format("round",shipsGame.getRounds()),labelStyle);
        textTable = new Table();
        textTable.setBounds(0, Gdx.graphics.getHeight() / 2f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 2f);
        textTable.add(showPlayersBoard).row();
        textTable.add(roundNumberLabel).row();
        textTable.add(yourShipsLeftLabel).row();
        textTable.add(shipsLeftLabel).row();
        textTable.add(selectedFieldLabel).row();
        textTable.add(resultLabel).row();
    }

    private void setupStage() {
        stage = new Stage();
        stage.addActor(exitButton);
        stage.addActor(textTable);
        stage.addActor(shipsGame.getBoards()[opponent.geId()].getBoardTable());
        stage.addListener(new PlayerSelectedFieldEventListener() {
            @Override
            public void select(PlayerSelectedFieldEvent event) {
                field = event.getSelectedField();
                playResult = play(player, opponent);
                selected = true;
                millisStart = TimeUtils.millis();
            }
        });
        stage.addActor(shipsGame.getPlayers()[0]);
        stage.addActor(shipsGame.getPlayers()[1]);
    }
}

package com.michal5111.ships.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.michal5111.ships.ShipsGame;

/**
 * Created by Michal on 07.11.2017.
 */

class SettingsScreen implements Screen{
    private final ShipsGame shipsGame;
    private final Stage stage;
    private final Table table;
    private final CheckBox checkBox;

    public SettingsScreen(ShipsGame game, final Screen returnScreen, final Stage returnStage) {
        Image background = new Image(new Texture("backgroundMenu.png"));
        background.setBounds(0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        this.shipsGame = game;
        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);
        table = new Table();
        table.setFillParent(true);
        stage.addActor(background);
        stage.addActor(table);
        checkBox = new CheckBox(shipsGame.getBundle().get("music"),shipsGame.getStylist().getCheckBoxStyle());
        checkBox.setChecked(shipsGame.getSoundManager().getBGMusic().isPlaying());
        TextButton textButton = new TextButton(shipsGame.getBundle().get("back"),shipsGame.getStylist().getTextButtonStyle());
        final Slider sliderMusic = new Slider(0,1,0.1f,false,shipsGame.getStylist().getSliderStyle());
        sliderMusic.setValue(shipsGame.getSoundManager().getBGMusic().getVolume());
        final Slider sliderEffects = new Slider(0,1,0.1f,false,shipsGame.getStylist().getSliderStyle());
        sliderEffects.setValue(shipsGame.getSoundManager().getEffectsVolume());
        Label musicLabel = new Label(shipsGame.getBundle().get("musicVolume"),shipsGame.getStylist().getLabelStyle());
        Label effectsLabel = new Label(shipsGame.getBundle().get("effectsVolume"),shipsGame.getStylist().getLabelStyle());
        table.add(checkBox).expandX().fill().row();
        table.add(musicLabel).row();
        table.add(sliderMusic).expandX().fill().height(100).row();
        table.add(effectsLabel).row();
        table.add(sliderEffects).expandX().fill().height(100).row();
        table.add(textButton);
        checkBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(!checkBox.isChecked()) {
                    shipsGame.getSoundManager().getBGMusic().stop();
                    shipsGame.getPreferences().putBoolean("musicOn",false);
                } else {
                    shipsGame.getSoundManager().getBGMusic().play();
                    shipsGame.getPreferences().putBoolean("musicOn",true);
                }
            }
        });
        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                shipsGame.getPreferences().flush();
                dispose();
                Gdx.input.setInputProcessor(returnStage);
                SettingsScreen.this.shipsGame.setScreen(returnScreen);
            }
        });
        sliderMusic.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                shipsGame.getSoundManager().getBGMusic().setVolume(sliderMusic.getValue());
                shipsGame.getPreferences().putFloat("musicVolume",sliderMusic.getValue());
            }
        });
        sliderEffects.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                shipsGame.getSoundManager().setEffectsVolume(sliderEffects.getValue());
                shipsGame.getSoundManager().getExplosion().play(sliderEffects.getValue());
                shipsGame.getPreferences().putFloat("effectsVolume",sliderEffects.getValue());
            }
        });
    }


    @Override
    public void show() {
        //Noting for now.
    }

    @Override
    public void render(float delta) {
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        //Noting for now.
    }

    @Override
    public void pause() {
        //Noting for now.
    }

    @Override
    public void resume() {
        //Noting for now.
    }

    @Override
    public void hide() {
        //Noting for now.
    }

    @Override
    public void dispose() {
        stage.dispose();
        table.remove();
    }
}

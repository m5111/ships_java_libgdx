package com.michal5111.ships.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.michal5111.ships.ShipsGame;

/**
 * Created by Michal on 07.11.2017.
 */

class MenuScreen implements Screen{
    private final ShipsGame shipsGame;
    private final TextButton playButton;
    private final Stage stage;
    private final Table table;

    public MenuScreen(final ShipsGame shipsGame) {
        Image background = new Image(shipsGame.getAssetManager().get("backgroundMenu.png", Texture.class));
        background.setBounds(0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        playButton = new TextButton(shipsGame.getBundle().get("play"), shipsGame.getStylist().getTextButtonStyle());
        TextButton exitButton = new TextButton(shipsGame.getBundle().get("exit"), shipsGame.getStylist().getTextButtonStyle());
        TextButton settingsButton = new TextButton(shipsGame.getBundle().get("settings"), shipsGame.getStylist().getTextButtonStyle());
        this.shipsGame = shipsGame;
        stage = new Stage(new ScreenViewport());
        Gdx.input.setInputProcessor(stage);
        table = new Table();
        table.setFillParent(true);
        table.add(playButton).expand().bottom().center().padRight(Gdx.graphics.getWidth()/5f).padLeft(Gdx.graphics.getWidth()/5f).height(Gdx.graphics.getHeight()/5f).fillX();
        table.row();
        table.add(settingsButton).expand().bottom().center().padRight(Gdx.graphics.getWidth()/5f).padLeft(Gdx.graphics.getWidth()/5f).height(Gdx.graphics.getHeight()/5f).fillX();
        table.row();
        table.add(exitButton).expand().bottom().center().padRight(Gdx.graphics.getWidth()/5f).padLeft(Gdx.graphics.getWidth()/5f).height(Gdx.graphics.getHeight()/5f).fillX();
        stage.addActor(background);
        stage.addActor(table);
        playButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                dispose();
                MenuScreen.this.shipsGame.setScreen(new PlayerSelectMenuScreen(MenuScreen.this.shipsGame));
            }
        });
        settingsButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                MenuScreen.this.shipsGame.setScreen(new SettingsScreen(shipsGame,MenuScreen.this,stage));
            }
        });
        exitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });
    }


    @Override
    public void show() {
        //Noting for now.
    }

    @Override
    public void render(float delta) {
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        //Noting for now.
    }

    @Override
    public void pause() {
        //Noting for now.
    }

    @Override
    public void resume() {
        //Noting for now.
    }

    @Override
    public void hide() {
        //Noting for now.
    }

    @Override
    public void dispose() {
        playButton.remove();
        stage.dispose();
        table.remove();
    }
}

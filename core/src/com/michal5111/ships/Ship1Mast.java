package com.michal5111.ships;

class Ship1Mast extends Ship {

    Ship1Mast(ShipsGame shipsGame, Board board) {
        //noinspection SpellCheckingInspection
        super(shipsGame,board, 1, "jednomasztowy");
    }
}

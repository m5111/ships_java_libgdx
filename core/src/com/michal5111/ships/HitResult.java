package com.michal5111.ships;

public enum HitResult {
    MISS, HIT, HIT_AND_SUNK
}

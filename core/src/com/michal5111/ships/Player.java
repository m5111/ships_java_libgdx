package com.michal5111.ships;

import com.badlogic.gdx.scenes.scene2d.Actor;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public abstract class Player extends Actor{

    final Ship[] ships;

    private final int id;
    Board notMyBoard;

    private Board myBoard;

    Player(ShipsGame shipsGame, Board[] boards, int id) {
        this.id = id;
        if (id == 0) {
            myBoard = boards[0];
            notMyBoard = boards[1];
        }
        if (id == 1) {
            myBoard = boards[1];
            notMyBoard = boards[0];
        }
        ships = new Ship[9];
        ships[0] = new Ship4Mast(shipsGame, myBoard);
        ships[1] = new Ship3Mast(shipsGame, myBoard);
        ships[2] = new Ship3Mast(shipsGame, myBoard);
        ships[3] = new Ship2Mast(shipsGame, myBoard);
        ships[4] = new Ship2Mast(shipsGame, myBoard);
        ships[5] = new Ship2Mast(shipsGame, myBoard);
        ships[6] = new Ship1Mast(shipsGame, myBoard);
        ships[7] = new Ship1Mast(shipsGame, myBoard);
        ships[8] = new Ship1Mast(shipsGame, myBoard);
    }
    public final HitResult hit(Point p) {
        if (notMyBoard.getMastAt(p).isHit()) {
            throw new IllegalArgumentException("Point is already hit");
        }
        notMyBoard.getMastAt(p).hit();
        if (notMyBoard.getMastAt(p).isOwned()) {
            if (notMyBoard.getMastAt(p).getOwner().isSunk()) {
                onHit(HitResult.HIT_AND_SUNK, p);
                return HitResult.HIT_AND_SUNK;
            }
            onHit(HitResult.HIT, p);
            return HitResult.HIT;
        }
        onHit(HitResult.MISS, p);
        return HitResult.MISS;
    }

    protected abstract void onHit(HitResult result, Point p);

    protected abstract Point getXY();

    protected abstract Direction getDirection();

    @Contract(pure = true)
    public final int geId() {
        return id;
    }

    public final boolean didLose() {
        for (Ship ship : ships) {
            if (!ship.isSunk()) {
                return false;
            }
        }
        return true;
    }

    @NotNull
    final Boolean setShip(int number) {
        Direction direction;
        boolean result = false;
        while (!result) {
            Point p = new Point(getXY());
            if (ships[number].getSize() == 1) {
                direction = Direction.UP;
            } else {
                direction = getDirection();
            }
            result = ships[number].set(p, direction);

            if (!result) {
                return false;
            }
        }
        return true;
    }

    public abstract void setShips();

    @Contract(pure = true)
    private Ship[] getShips() {
        return ships;
    }

    public int getUnsunkenShipsCount() {
        int counter = 0;
        for (Ship ship: getShips()) {
            if(!ship.isSunk()) {
                counter++;
            }
        }
        return counter;
    }

    public Board getMyBoard() {
        return myBoard;
    }

}

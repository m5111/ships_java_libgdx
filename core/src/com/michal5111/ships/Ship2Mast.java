package com.michal5111.ships;

class Ship2Mast extends Ship {

    Ship2Mast(ShipsGame shipsGame, Board board) {
        //noinspection SpellCheckingInspection
        super(shipsGame, board, 2, "dwumasztowy");
    }
}

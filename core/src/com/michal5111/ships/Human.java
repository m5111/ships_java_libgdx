package com.michal5111.ships;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.michal5111.ships.events.ClickedFieldEventListener;
import com.michal5111.ships.events.PlayerSelectedFieldEventListener;
import com.michal5111.ships.events.SelectFieldEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.Random;

public class Human extends Player {

    private final Random random;
    private Boolean select;

    public Human(final ShipsGame shipsGame, Board[] boards, final int id) {
        super(shipsGame, boards, id);
        BitmapFont font = new BitmapFont();
        font.getData().setScale(Gdx.graphics.getDensity());
        random = new Random();
        select = false;
        addListener(new SelectFieldEventListener() {
            @Override
            public void selected(Stage stage) {
                select = true;
            }
        });
        addListener(new ClickedFieldEventListener() {
            @Override
            public void clicked(Event event, Stage stage) {
                if (select) {
                    Point p = ((ClickedFieldEvent) event).getClickedPoint();
                    if (!notMyBoard.getMastAt(p.getX(),p.getY()).isHit()) {
                        stage.getRoot().fire(new PlayerSelectedFieldEventListener.PlayerSelectedFieldEvent(p));
                        select = false;
                    }
                }
            }
        });
    }

    @Override
    protected void onHit(HitResult result, Point p) {
        //Nothing for now
    }

    @NotNull
    @Override
    public final Point getXY() {
        int x;
        int y;
        do {
            x = random.nextInt(Constants.BOARD_SIZE);
            y = random.nextInt(Constants.BOARD_SIZE);
        } while (notMyBoard.getMastAt(x,y).isHit());
        return new Point(x, y);
    }

    @Override
    public final Direction getDirection() {
        return Direction.values()[random.nextInt(4)];
    }

    @Override
    public void setShips() {
        for (int i = 0; i < ships.length; i++) {
            while (true) {
                if (setShip(i)) {
                    break;
                }
            }
        }
    }
}

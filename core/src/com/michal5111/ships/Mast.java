package com.michal5111.ships;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.michal5111.ships.events.ClickedFieldEventListener;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public class Mast extends Image{

    private boolean hit;
    private Ship ship;
    private final Point pos;
    private final ShipsGame shipsGame;
    private final Texture emptyField;
    private final Texture emptyHit;
    private final Texture shipFieldHit;
    private final Sound explosion;

    Mast(final Point pos,ShipsGame shipsGame) {
        this.pos = pos;
        this.shipsGame = shipsGame;
        this.hit = false;
        this.ship = null;
        emptyField = shipsGame.getAssetManager().get("emptyField.png");
        emptyHit = shipsGame.getAssetManager().get("emptyFieldHit.png");
        shipFieldHit = shipsGame.getAssetManager().get("shipFieldHit.png");
        setDrawable(changeTexture());
        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                for (Actor actor : event.getStage().getActors()) {
                    actor.fire(new ClickedFieldEventListener.ClickedFieldEvent(getPos()));
                }
            }
        });
        explosion = shipsGame.getSoundManager().getExplosion();
    }

    final void hit() {
        hit = true;
        setDrawable(changeTexture());
        explosion.play(shipsGame.getSoundManager().getEffectsVolume());
    }

    @Contract(pure = true)
    final boolean isHit() {
        return hit;
    }

    @Contract(pure = true)
    final boolean isOwned() {
        return ship != null;
    }

    @Contract(pure = true)
    final Ship getOwner() {
        return ship;
    }

    final void setOwner(Ship ship) {
        this.ship = ship;
        setDrawable(changeTexture());
    }

    @NotNull
    private SpriteDrawable changeTexture() {
        if (isHit()) {
            if( isOwned()) {
                return new SpriteDrawable(new Sprite(shipFieldHit));
            } else {
                return new SpriteDrawable(new Sprite(emptyHit));
            }
        } else {
            return new SpriteDrawable(new Sprite(emptyField));
        }
    }

    @Contract(pure = true)
    private Point getPos() {
        return pos;
    }
}

package com.michal5111.ships;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.michal5111.ships.events.PlayerSelectedFieldEventListener;
import com.michal5111.ships.events.SelectFieldEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class Computer extends Player {

    private final Random random = new Random();
    private final Deque<Point> pointsToCheck;
    private final Queue<Point> nextHits;
    private Point firstHit;
    private Point secondHit;
    private states state;

    private enum states {
        RANDOM, SEEK, DESTROY
    }

    private int direction = 0;

    public Computer(ShipsGame shipsGame, Board[] boards, int id) {
        super(shipsGame, boards, id);
        pointsToCheck = new LinkedList<Point>();
        nextHits = new LinkedList<Point>();
        state = states.RANDOM;
        this.addListener(new SelectFieldEventListener() {
            @Override
            public void selected(Stage stage) {
                stage.getRoot().fire(new PlayerSelectedFieldEventListener.PlayerSelectedFieldEvent(getXY()));
            }
        });
    }

    @NotNull
    private Boolean checkPointIsNextToSunkenShip(Point p) {
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (!Point.checkPoint(p.getX() + i, p.getY() + j) || notMyBoard.getMastAt(p.getX() + i,p.getY() + j).getOwner() == null)
                    continue;
                if (notMyBoard.getMastAt(p.getX() + i,p.getY() + j).getOwner().isSunk()) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onHit(HitResult result, Point p) {
        if (result == HitResult.HIT) {
            if (state == states.SEEK) {
                pointsToCheck.clear();
                state = states.DESTROY;
                secondHit = p;
            } else if (state == states.RANDOM) {
                state = states.SEEK;
                pointsToCheck.addAll(getNeighbours(p));
                firstHit = p;
            }
        } else if (result == HitResult.HIT_AND_SUNK) {
            pointsToCheck.clear();
            nextHits.clear();
            state = states.RANDOM;
        } else if (state == states.DESTROY && result == HitResult.MISS) {
            nextHits.clear();
            flipDirection();
        }
    }

    private Deque<Point> getNeighbours(Point p) {
        Deque<Point> deque = new LinkedList<Point>();
        for (int y = -1; y <= 1; y++) {
            for (int x = -1; x <= 1; x++) {
                Point temp = new Point(p.getX() + x,p.getY() + y);
                if (Math.abs(x) == Math.abs(y) || !Point.checkPoint(temp) || notMyBoard.getMastAt(temp).isHit() || !checkPointIsNextToSunkenShip(temp))
                    continue;
                deque.add(new Point(p.getX() + x, p.getY() + y));
            }
        }
        return deque;
    }

    private Point stateSeek() {
        if (!pointsToCheck.isEmpty()) {
            return pointsToCheck.pop();
        } else {
            state = states.RANDOM;
            return stateRandom();
        }
    }

    @NotNull
    private Point stateRandom() {
        int x;
        int y;
        do {
            x = random.nextInt(Constants.BOARD_SIZE);
            y = random.nextInt(Constants.BOARD_SIZE);
        } while (notMyBoard.getMastAt(x,y).isHit() || !checkPointIsNextToSunkenShip(new Point(x, y)));
        return new Point(x, y);
    }

    @Override
    protected final Point getXY() {
        Point p = new Point(0, 0);
        if (state == states.RANDOM) {
            p = stateRandom();
        } else if (state == states.SEEK) {
            p = stateSeek();
        } else if (state == states.DESTROY) {
            p = stateDestroy();
        }
        return p;
    }

    private void flipDirection() {
        direction = direction == 1 ? -1 : 1;
    }

    private void addNextHits(Boolean vertical) {
        Point orientation = vertical ? new Point(1, 0) : new Point(0, 1);
        for (int i = 1; i <= 3; i++) {
            Point temp = new Point(secondHit.getX() + i*orientation.getX()*direction, secondHit.getY() + i*orientation.getY()*direction);
            if (Point.checkPoint(temp)) {
                if (notMyBoard.getMastAt(temp).isHit() && notMyBoard.getMastAt(temp) != notMyBoard.getMastAt(firstHit))
                    break;
                if (!notMyBoard.getMastAt(temp).isHit() && !temp.equals(firstHit) && checkPointIsNextToSunkenShip(temp))
                    nextHits.add(temp);
            }
        }
        if(nextHits.isEmpty()) {
            flipDirection();
            addNextHits(vertical);
        }
    }

    private Point stateDestroy() {
        if (nextHits.isEmpty()) {
            if (firstHit.getX() == secondHit.getX()) {
                addNextHits(false);
            } else if (firstHit.getY() == secondHit.getY()) {
                addNextHits(true);
            }
        }
        return nextHits.poll();
    }

    @Override
    public final Direction getDirection() {
        return Direction.values()[random.nextInt(4)];
    }

    @Override
    public void setShips() {
        for (int i = 0; i < ships.length; i++) {
            while (true) {
                if (setShip(i)) {
                    break;
                }
            }

        }
    }
}
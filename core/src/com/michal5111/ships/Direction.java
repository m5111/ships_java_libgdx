package com.michal5111.ships;

public enum Direction {
    UP(new Point(-1,0)), DOWN(new Point(1,0)), LEFT(new Point(0,-1)), RIGHT(new Point(0,1));

    Direction(Point point) {
        this.point = point;
    }
    private final Point point;

    public Point getPoint() {
        return point;
    }
}

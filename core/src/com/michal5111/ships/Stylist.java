package com.michal5111.ships;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

/**
 * Created by Michal on 15.11.2017.
 */

public class Stylist {
    private final TextButton.TextButtonStyle textButtonStyle;

    private final BitmapFont font;
    private final CheckBox.CheckBoxStyle checkBoxStyle;
    private final Label.LabelStyle labelStyle;
    private final TextureAtlas atlas;
    private final Skin skin;
    private final Slider.SliderStyle sliderStyle;
    private final Button.ButtonStyle buttonStyle;

    public Stylist(AssetManager assetManager) {
        textButtonStyle = new TextButton.TextButtonStyle();
        font = new BitmapFont();
        labelStyle = new Label.LabelStyle();
        labelStyle.font = font;
        atlas = assetManager.get("ui-red.atlas");
        skin = new Skin();
        skin.addRegions(atlas);
        font.getData().setScale(Gdx.graphics.getDensity()*2);
        textButtonStyle.font = font;
        textButtonStyle.up = skin.getDrawable("button_01");
        textButtonStyle.down = skin.getDrawable("button_02");
        textButtonStyle.checked = skin.getDrawable("button_03");
        checkBoxStyle = new CheckBox.CheckBoxStyle();
        checkBoxStyle.checkboxOff = skin.getDrawable("checkbox_off");
        checkBoxStyle.checkboxOn = skin.getDrawable("checkbox_on");
        checkBoxStyle.font = font;
        sliderStyle = new Slider.SliderStyle();
        sliderStyle.knob = skin.getDrawable("knob_01");
        sliderStyle.knobDown = skin.getDrawable("knob_01");
        sliderStyle.background = skin.getDrawable("slider_back_hor");
        final String buttonCross = "button_cross";
        buttonStyle = new Button.ButtonStyle(skin.getDrawable(buttonCross),skin.getDrawable(buttonCross),skin.getDrawable(buttonCross));
    }

    public TextButton.TextButtonStyle getTextButtonStyle() {
        return textButtonStyle;
    }

    public BitmapFont getFont() {
        return font;
    }

    public CheckBox.CheckBoxStyle getCheckBoxStyle() {
        return checkBoxStyle;
    }

    public Label.LabelStyle getLabelStyle() {
        return labelStyle;
    }

    public Skin getSkin() {
        return skin;
    }

    public Slider.SliderStyle getSliderStyle() {
        return sliderStyle;
    }

    public void dispose() {
        skin.dispose();
        atlas.dispose();
        font.dispose();
    }

    public Button.ButtonStyle getButtonStyle() {
        return buttonStyle;
    }
}

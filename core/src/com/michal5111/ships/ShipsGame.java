package com.michal5111.ships;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.I18NBundle;
import com.michal5111.ships.screens.LoadScreen;

/**
 * Created by Michal on 08.11.2017.
 */

public class ShipsGame extends Game {
    private int turn;
    private int rounds;

    private Board[] boards;

    private Player[] players;
    private Stylist stylist;
    private AssetManager assetManager;
    private SoundManager soundManager;
    private Preferences preferences;
    private I18NBundle bundle;
    @Override
    public void create() {
        turn = 0;
        rounds = 0;
        assetManager = new AssetManager();
        this.setScreen(new LoadScreen(this));
    }

    public Player[] getPlayers() {
        return players;
    }

    public int getTurn() {
        return turn;
    }

    public void switchTurn() {
        turn = turn == 0 ? 1 : 0;
    }

    @Override
    public void dispose() {
        assetManager.dispose();
        stylist.dispose();
        soundManager.dispose();
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    public void afterLoading() {
        preferences = Gdx.app.getPreferences("Preferences");
        soundManager = new SoundManager(this);
        bundle = assetManager.get("MyBundle");
        stylist = new Stylist(assetManager);
        boards = new Board[2];
        for (int i = 0; i < boards.length; i++) {
            boards[i] = new Board(this,Constants.BOARD_SIZE,Constants.BOARD_SIZE);
        }
        players = new Player[2];
    }

    public Stylist getStylist() {
        return stylist;
    }

    public SoundManager getSoundManager() {
        return soundManager;
    }

    public Preferences getPreferences() {
        return preferences;
    }

    public int getRounds() {
        return rounds;
    }

    public void incrementRounds() {
        rounds++;
    }

    public I18NBundle getBundle() {
        return bundle;
    }

    public Board[] getBoards() {
        return boards;
    }
}

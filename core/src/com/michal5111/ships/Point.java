package com.michal5111.ships;

import org.jetbrains.annotations.Contract;

public class Point {

    private final int x;
    private final int y;

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    Point(Point p) {
        x = p.getX();
        y = p.getY();
    }

    @Contract(pure = true)
    public final int getX() {
        return x;
    }

    @Contract(pure = true)
    public final int getY() {
        return y;
    }

    public String toString() {
        return String.valueOf((char) (x + 65)) + (y + 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Contract(pure = true)
    static boolean checkPoint(int x, int y) {
        return !(x < 0 || x > Constants.BOARD_SIZE - 1 || y < 0 || y > Constants.BOARD_SIZE - 1);
    }

    static boolean checkPoint(Point p) {
        return !(p.getX() < 0 || p.getX() > Constants.BOARD_SIZE - 1 || p.getY() < 0 || p.getY() > Constants.BOARD_SIZE - 1);
    }
}

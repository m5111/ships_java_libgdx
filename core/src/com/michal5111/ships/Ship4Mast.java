package com.michal5111.ships;

class Ship4Mast extends Ship {

    Ship4Mast(ShipsGame shipsGame, Board board) {
        //noinspection SpellCheckingInspection
        super(shipsGame, board, 4, "czteromasztowy");
    }
}

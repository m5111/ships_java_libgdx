package com.michal5111.ships;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

class Ship {

    private final ShipsGame shipsGame;
    private final Board board;
    private final Mast[] masts;
    private final int size;
    private final String name;

    Ship(ShipsGame shipsGame, Board board, int size, String name) {
        this.shipsGame = shipsGame;
        this.board = board;
        this.size = size;
        this.name = name;
        masts = new Mast[size];
    }

    @NotNull
    private Boolean checkPlace(int x, int y) {
        for (int i = x - 1; i <= x + 1; i++)
            for (int j = y - 1; j <= y + 1; j++)
                if (Point.checkPoint(i, j) && board.getMastAt(i,j).getOwner() != null && board.getMastAt(i,j).getOwner() != this) {
                    return false;
                }
        return true;
    }

    final boolean set(Point p, Direction direction) {
        Point[] pointsToCheck = new Point[size];
        pointsToCheck[0] = p;
        for (int i = 1; i < size; i++) {
            pointsToCheck[i] = new Point(p.getX()+i*direction.getPoint().getX(),p.getY()+i*direction.getPoint().getY());
        }
        for(Point i: pointsToCheck) {
            if (!Point.checkPoint(i.getX(),i.getY()) || !checkPlace(i.getX(), i.getY()))
                return false;
        }
        for(int i = 0; i < size; i++) {
            masts[i] = board.getMastAt(pointsToCheck[i]);
            masts[i].setOwner(this);
        }
        return true;
    }

    final boolean isSunk() {
        for (int i = 0; i < size; i++) {
            if (!masts[i].isHit()) {
                return false;
            }
        }
        for (Mast mast: masts) {
            mast.setDrawable(new SpriteDrawable(new Sprite(shipsGame.getAssetManager().get("shipFieldSunk.png", Texture.class))));
        }
        return true;
    }

    @Contract(pure = true)
    final int getSize() {
        return size;
    }

    @Contract(pure = true)
    final String getName() {
        return name;
    }
}

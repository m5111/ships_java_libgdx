package com.michal5111.ships;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * Created by Michal on 16.11.2017.
 */

public class SoundManager {
    private final Music bGMusic;
    private final Sound explosion;
    private float effectsVolume;

    public SoundManager(ShipsGame shipsGame) {
        explosion = shipsGame.getAssetManager().get("explosion.ogg");
        effectsVolume = shipsGame.getPreferences().contains("effectsVolume") ? shipsGame.getPreferences().getFloat("effectsVolume") : 1;
        bGMusic = shipsGame.getAssetManager().get("bGMusic.ogg");
        bGMusic.setLooping(true);
        bGMusic.setVolume(shipsGame.getPreferences().contains("musicVolume") ? shipsGame.getPreferences().getFloat("musicVolume") : 0.5f);
        if(!shipsGame.getPreferences().contains("musicOn") || shipsGame.getPreferences().getBoolean("musicOn")) {
            bGMusic.play();
        }
    }

    public Music getBGMusic() {
        return bGMusic;
    }

    public Sound getExplosion() {
        return explosion;
    }

    public void dispose() {
        bGMusic.dispose();
        explosion.dispose();
    }

    public float getEffectsVolume() {
        return effectsVolume;
    }

    public void setEffectsVolume(float effectsVolume) {
        this.effectsVolume = effectsVolume;
    }
}

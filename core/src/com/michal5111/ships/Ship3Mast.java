package com.michal5111.ships;

class Ship3Mast extends Ship {

    Ship3Mast(ShipsGame shipsGame, Board board) {
        //noinspection SpellCheckingInspection
        super(shipsGame, board, 3, "trzymasztowy");
    }
}
